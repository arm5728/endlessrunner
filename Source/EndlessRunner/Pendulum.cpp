// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "Components/SphereComponent.h"
#include "Pendulum.h"

// Sets default values
APendulum::APendulum()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMesh(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMesh(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));

	// pendulum mesh which is a sphere
	PendulumMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PendulumMesh"));
	PendulumMesh->SetStaticMesh(SphereMesh.Object);
	PendulumMesh->SetSimulatePhysics(true);
	RootComponent = PendulumMesh;

	// collision
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->InitSphereRadius(50);
	SphereComponent->SetupAttachment(RootComponent);
	SphereComponent->BodyInstance.SetCollisionProfileName("Sphere");

	// physics constraint that will make the pendulum go in the direction
	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraint"));
	PhysicsConstraint->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void APendulum::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APendulum::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

