// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UStaticMeshComponent;

UCLASS()
class ENDLESSRUNNER_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Spring Arm Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* SpringArm;

	// Camera Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent* CameraComponent;

	// Base Mesh
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player")
	UStaticMeshComponent * MeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Move Right or Left
	void MoveRight(float Value);

	// Jump
	void Jumping();

	// Reset Game on Death
	UFUNCTION(BlueprintCallable)
	void GameOver();

	UFUNCTION(BlueprintImplementableEvent)
	void ShowScore();

	UFUNCTION(BlueprintImplementableEvent)
	void ResetCoins();

	UFUNCTION(BlueprintImplementableEvent)
	void GameOverSound();
	
	UFUNCTION(BlueprintImplementableEvent)
	void JumpingSound();

	UFUNCTION(BlueprintCallable)
	void RestartGame();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Basic Game State (Alive or Dead)
	UPROPERTY(VisibleAnywhere, Category = "GameState")
	bool PlayerAlive = true;

	// Delay to Restart Game
	UPROPERTY(EditAnywhere, Category = "GameState")
	float GameOverWaitTime = 5.0f; //seconds

	//Starting Position
	FVector StartingPosition = FVector(0.0f, 0.0f, 125.0f);

	//Save time of Game Over
	float GameOverTime;

	//BELOW: Variables Affecting Constant Movement Forward
	UPROPERTY(EditAnywhere, Category = "MoveForward")
	float AccelerationRate = 1.0f;

	UPROPERTY(EditAnywhere, Category = "MoveForward")
	float StartingVelocity = 15.0f;

	UPROPERTY(EditAnywhere, Category = "MoveForward")
	float MaxVelocity = 20.0f;

	//BELOW: Variables Affecting Player-Controlled Sideways Movement
	UPROPERTY(EditAnywhere, Category = "MoveRight")
	float Force = 1000.0f;

	float CurrentVelocity = StartingVelocity;

	//BELOW: Variables Affecting Player-Controlled Jump
	UPROPERTY(EditAnywhere, Category = "Jumping")
	float JumpForce = 1500.0f;

	//Adjustable Cooldown Time for Jump
	UPROPERTY(EditAnywhere, Category = "Jumping")
	float JumpCooldown = 1.0f; //seconds

	//Save time of last jump
	float TimeOfLastJump = 0.0f;
};
