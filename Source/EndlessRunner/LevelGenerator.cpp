// Fill out your copyright notice in the Description page of Project Settings.

#include "Obstacle.h"
#include "Engine/World.h"
#include "LevelGenerator.h"

// Sets default values
ALevelGenerator::ALevelGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALevelGenerator::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelGenerator::GenerateSection(float x, int difficulty)
{
	int sect = FMath::RandRange(0, FMath::Min(2 + difficulty,8));
	//GEngine->AddOnScreenDebugMessage(-1, 200, FColor::Green, FString::Printf(TEXT("Hello %i"), sect));
	GetWorld()->SpawnActor<AActor>(CoinClass, FVector(0.0f + x + FMath::FRandRange(-200, -1200), FMath::FRandRange(-320, 320), 150.0f), FRotator(0.0f, 0.0f, 0.0f));
	GetWorld()->SpawnActor<AActor>(CoinClass, FVector(0.0f + x + FMath::FRandRange(-1700, -3000), FMath::FRandRange(-320, 320), 150.0f), FRotator(0.0f, 0.0f, 0.0f));

	switch (sect) {
		case 0: // Hurdle
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[6], FVector(4.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			break;
		}
		case 1: // Pillars
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[0], FVector(1.0f * -400 + x, FMath::RandRange(-320.0f, 320.0f), 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[0], FVector(2.5f * -400 + x, FMath::RandRange(-320.0f, 320.0f), 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[0], FVector(4.0f * -400 + x, FMath::RandRange(-320.0f, 320.0f), 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[0], FVector(5.5f * -400 + x, FMath::RandRange(-320.0f, 320.0f), 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[0], FVector(7.0f * -400 + x, FMath::RandRange(-320.0f, 320.0f), 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			break;
		}
		case 2: // UpDown
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			AActor* a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[5], FVector(4.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			break;
		}
		case 3: // LeftRight
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			AActor* a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[2], FVector(4.0f * -400 + x, -150.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			break;
		}
		case 4: // ZigZag
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(4.0f, 1.0f, 2.0f));
			floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(4.0f * -400 + x, 0.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(4.0f, 1.0f, 2.0f));
			break;
		}
		case 5: // Circle
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			AActor* a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[1], FVector(2.5f * -400 + x, -400.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[1], FVector(5.5f * -400 + x, -400.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			break;
		}
		case 6: // Rotate V
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			AActor* a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[4], FVector(2.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[4], FVector(6.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			break;
		}
		case 7: // Rotate H
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(8.0f, 2.0f, 2.0f));
			AActor* a = GetWorld()->SpawnActor<AActor>(ObstacleClasses[3], FVector(2.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			((AObstacle*)a)->startDelay = FMath::FRand() * 2;
			GetWorld()->SpawnActor<AActor>(ObstacleClasses[7], FVector(6.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			break;
		}
		case 8: // Platforms
		{
			AActor* floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(0.0f + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			floor->SetActorScale3D(FVector(4.0f, 2.0f, 2.0f));
			GetWorld()->SpawnActor<AActor>(PlatformClasses[0], FVector(8.0f * -400 + x, -400.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			GetWorld()->SpawnActor<AActor>(PlatformClasses[1], FVector(8.0f * -400 + x, 0.0f, 70.0f), FRotator(0.0f, 0.0f, 0.0f));
			//floor = GetWorld()->SpawnActor<AActor>(FloorClass, FVector(7.0f * -400 + x, -400.0f, 32.0f), FRotator(-180.0f, 0.0f, 0.0f));
			//floor->SetActorScale3D(FVector(1.0f, 2.0f, 2.0f));
			break;
		}
		
		
	}
	
}