// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArm);

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlayerMesh"));
	MeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentVelocity = StartingVelocity;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Check if Game Not Over
	if (PlayerAlive) {
		//Add Constant Forward Acceleration
		FVector Location = GetActorLocation();
		if (CurrentVelocity < MaxVelocity)
			CurrentVelocity += AccelerationRate * DeltaTime;
		Location += GetActorForwardVector() * CurrentVelocity;
		SetActorLocation(Location);  

		//Check if player has fallen off level
		FVector SecondLocation = GetActorLocation();
		if (SecondLocation.Z < 0.0f) {
			//Start GameOver Procedure
			GameOver();
		}
	} else {
		//If GameOver Delay Time is Over, Restart Game
		if (GameOverTime + GameOverWaitTime <= GetGameTimeSinceCreation()) {
			RestartGame();
		}
	}

}

// Move Right or Left
void APlayerCharacter::MoveRight(float Value)
{
	if((Controller) && (Value != 0.0f) && PlayerAlive)
	{
		ShowScore();
		FVector Right = GetActorRightVector();
		if (Value < 0.0f) {
			Right.Y = Right.Y * -1.0f;
		}

		MeshComponent->AddForce(Right * Force * MeshComponent->GetMass());
	}
}

// Jump
void APlayerCharacter::Jumping()
{
	if (((TimeOfLastJump == 0.0f) || (TimeOfLastJump + JumpCooldown <= GetGameTimeSinceCreation())) && PlayerAlive){
		FVector Up = GetActorUpVector();
		MeshComponent->AddImpulse(Up * JumpForce * MeshComponent->GetMass());
		TimeOfLastJump = GetGameTimeSinceCreation();
	}
	JumpingSound();
}

// Game Over Procedure
void APlayerCharacter::GameOver()
{
	CurrentVelocity = StartingVelocity;
	GameOverSound();
	GameOverTime = GetGameTimeSinceCreation();
	PlayerAlive = false;
	MeshComponent->SetNotifyRigidBodyCollision(false);
}

// Restart Game Procedure
void APlayerCharacter::RestartGame()
{
	PlayerAlive = true;
	SetActorLocation(StartingPosition);

	ResetCoins();

	FVector NewVelocity = FVector(0.0f, 0.0f, 0.0f);
	MeshComponent->SetPhysicsLinearVelocity(NewVelocity);

	MeshComponent->SetNotifyRigidBodyCollision(true);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Bind Jump Input (Space Bar)
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::Jumping);

	//Bind Move Right (A, D)
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
}

