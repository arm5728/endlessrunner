// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelGenerator.generated.h"

UCLASS()
class ENDLESSRUNNER_API ALevelGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelGenerator();

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<AActor>> ObstacleClasses;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> FloorClass;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<AActor>> PlatformClasses;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> CoinClass;

	UFUNCTION(BlueprintCallable)
		void GenerateSection(float x, int difficulty);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
